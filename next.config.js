/** @type {import('next').NextConfig} */

const prodConfig = {
  domainUrl: 'http://kpbamboejaya.com',
};

const devConfig = {
  domainUrl: 'http://localhost:3000',
};

module.exports = {
  reactStrictMode: true,
  eslint: {
    ignoreDuringBuilds: true,
  },
  generateBuildId: async () => {
    if (process.env.BUILD_ID) {
      return process.env.BUILD_ID;
    }
    return `${new Date().getTime()}`;
  },
  env: {
    baseUrl: process.env.NODE_ENV === 'production' ? prodConfig.domainUrl : devConfig.domainUrl,
  },

};
