import React, { ReactNode, useState } from 'react';
import { useRouter } from 'next/router';
import Head from 'next/head';
import Image from 'next/image';
import Link from 'next/link';

import {
  Container, Row, Col, Navbar, Nav,
} from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconProp } from '@fortawesome/fontawesome-svg-core';

interface LayoutProps {
  children: ReactNode;
  pageTitle: string;
  metaKeywords: string;
  metaDescription: string;
  metaImage?: string;
}
const MainLayout = function (LayoutProp: LayoutProps) {
  const [expanded, setExpanded] = useState(false);

  const {
    children, pageTitle, metaKeywords, metaDescription, metaImage,
  } = LayoutProp;

  // LayoutProp.metaImage = metaImage !== undefined ? metaImage : `${process.env.BASE_URL}/img/social-card.jpg`;

  const router = useRouter();
  const UrlPath = router.pathname;
  const UrlExplode = UrlPath.split('/');
  const UrlMainMenu = UrlExplode.slice(0, 2).join('/');

  React.useEffect(() => {
    const MainNavElm = document.getElementById('mainNav') as HTMLElement;
    const scrollHandler = () => {
      if (window.pageYOffset > 55) {
        MainNavElm.classList.add('scrolled');
      } else {
        MainNavElm.classList.remove('scrolled');
      }
    };
    window.addEventListener('scroll', scrollHandler);
    scrollHandler();
    return () => {
      window.removeEventListener('scroll', scrollHandler);
    };
  });

  const year = new Date().getFullYear();
  return (
    <>
      <Head>
        <title>
          {pageTitle}
        </title>
        <link rel="icon" href="/img/favicon.ico" />
        <meta name="keywords" content={metaKeywords} />
        <meta name="description" content={metaDescription} />
        <meta name="og:title" content={pageTitle} />
        <meta name="og:description" content={metaDescription} />
        <meta name="og:image" content={metaImage || `${process.env.BASE_URL}/img/social-card.jpg`} />
        <meta name="og:url" content={`${process.env.BASE_URL}${UrlPath}`} />
      </Head>

      <Navbar collapseOnSelect expand="lg" id="mainNav" sticky="top" expanded={expanded}>
        <Container>
          <Link href="/">

            <Navbar.Brand href="#home">
              <Image
                src="/img/logo.png"
                layout="intrinsic"
                width={60}
                height={48}
              />
            </Navbar.Brand>

          </Link>
          {/* <Navbar.Toggle aria-controls="MainMenuControll" className="text-light" children={<FontAwesomeIcon icon={['far', 'bars'] as IconProp} />} /> */}
          <Navbar.Toggle className="text-light" children={<FontAwesomeIcon icon={['far', 'bars'] as IconProp} />} onClick={() => setExpanded(!expanded)} />
          <Navbar.Collapse id="MainMenuControll">
            <Nav className="ms-auto">
              <div className="d-lg-none d-flex mt-2 border-bottom border-1 border-light mb-1 pb-1">
                <div className="w-100 my-auto ps-3">
                  <img
                    src="/img/logo.png"
                    alt="Bamboe Jaya Logo"
                    height={38}
                  />
                </div>
                <div className="flex-shrink-1 pe-2">
                  <Navbar.Toggle children={<FontAwesomeIcon icon={['far', 'times'] as IconProp} />} onClick={() => setExpanded(!expanded)} />
                </div>
              </div>
              <Link href="/">
                <a role="button" className={`nav-link ${UrlMainMenu === '/' ? 'active' : ''}`} tabIndex={0}>Beranda</a>
              </Link>
              <Link href="/tentang-kami">
                <a role="button" className={`nav-link ${UrlMainMenu === '/tentang-kami' ? 'active' : ''}`} tabIndex={0}>Tentang Kami</a>
              </Link>
              <Link href="/jadwal-kegiatan">
                <a role="button" className={`nav-link ${UrlMainMenu === '/jadwal-kegiatan' ? 'active' : ''}`} tabIndex={0}>Jadwal Kegiatan</a>
              </Link>
              <Link href="/kontak">
                <a role="button" className={`nav-link ${UrlMainMenu === '/kontak' ? 'active' : ''}`} tabIndex={0}>Kontak Kami</a>
              </Link>
              {/* <Link href="/kontak">
                <Button variant="outline-success" size="sm">Search</Button>
              </Link> */}

            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>

      {children}

      <footer className="bg-dark text-light py-3">
        <Container>
          <Row className="py-3">
            <Col md="4">
              <Image
                src="/img/logo-bordered.png"
                layout="intrinsic"
                width={100}
                height={85}
              />
              <p className="mb-0">Kolam Pemancingan BamboeJaya yang berada di Malang Jawa Timur ada sejak tahun 2016 hingga saat ini.</p>
            </Col>
            <Col md="2">
              <p className="fw-bold">TAUTAN CEPAT</p>
              <ul className="list-unstyled">
                <li>
                  <FontAwesomeIcon icon={['fal', 'chevron-right'] as IconProp} className="me-1" />
                  <Link href="/">
                    <a>Beranda</a>
                  </Link>
                </li>
                <li>
                  <FontAwesomeIcon icon={['fal', 'chevron-right'] as IconProp} className="me-1" />
                  <Link href="/tentang-kami">
                    <a>Tentang Kami</a>
                  </Link>
                </li>
                <li>
                  <FontAwesomeIcon icon={['fal', 'chevron-right'] as IconProp} className="me-1" />
                  <Link href="/jadwal-kegiatan">
                    <a>Jadwal Kegiatan</a>
                  </Link>
                </li>
                <li>
                  <FontAwesomeIcon icon={['fal', 'chevron-right'] as IconProp} className="me-1" />
                  <Link href="/kontak">
                    <a>Kontak Kami</a>
                  </Link>
                </li>
              </ul>
            </Col>
            <Col md="2">
              <p className="fw-bold">LOMBA MANCING</p>
              <ul className="list-unstyled">
                <li>
                  <FontAwesomeIcon icon={['fal', 'chevron-right'] as IconProp} className="me-1" />
                  <Link href="/jadwal-kegiatan/lomba-mancing-rilis-ikan-lele">
                    <a>RILIS LELE</a>
                  </Link>
                </li>
                <li>
                  <FontAwesomeIcon icon={['fal', 'chevron-right'] as IconProp} className="me-1" />
                  <Link href="/jadwal-kegiatan/lomba-mancing-ikan-mas">
                    <a>IKAN TOMBRO</a>
                  </Link>
                </li>

              </ul>
            </Col>
            <Col md="4">
              <p className="fw-bold">ALAMAT KAMI</p>
              <p>JL. Tamanu diharjo, Mbunder Ledok, Ampeldento, Karangploso, Kabupaten Malang</p>
              <p className="mb-0">
                <Link href="https://wa.me/6282331761443">
                  <a target="blank">
                    <FontAwesomeIcon icon={['fab', 'whatsapp-square'] as IconProp} className="me-1" />
                    {' '}
                    082-331-761-443
                  </a>
                </Link>
              </p>
              <p className="mb-0">
                <Link href="https://fb.me/bamboejayamalang">
                  <a target="blank">
                    <FontAwesomeIcon icon={['fab', 'facebook'] as IconProp} className="me-1" />
                    {' '}
                    bamboejayamalang
                  </a>
                </Link>
              </p>
            </Col>
          </Row>
          <hr />
          <p>
            {year}
            {' '}
            &copy; KP BamboeJaya
          </p>

        </Container>
      </footer>

    </>
  );
};

export default MainLayout;
