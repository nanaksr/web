import React from 'react';

// import Head from 'next/head';
import Image from 'next/image';

import {
  Carousel,
} from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconProp } from '@fortawesome/fontawesome-svg-core';

const Testimonials = function () {
  return (
    <Carousel controls={false}>
      <Carousel.Item interval={3000}>
        <div className="d-flex flex-md-row flex-column bd-highlight mb-3">
          <div className="p-2 text-center" style={{ minWidth: '160px' }}>
            <div className="position-relative">
              <Image
                src="/img/pakpur.jpg"
                layout="intrinsic"
                width={100}
                height={100}
                className="rounded-circle mx-auto d-block border"
              />
              <h5>Pak Pur</h5>
            </div>
          </div>
          <div className="p-2 bg-white mr-auto position-relative fst-italic shadow-sm">

            <FontAwesomeIcon className="fa-2x" icon={['fas', 'quote-left'] as IconProp} />
            <p className="mb-0">Saya tidak pernah absen untuk mengikut lomba memancing di BamboeJaya, karena kolamnya nyaman serta ikannya haup.</p>
          </div>
        </div>
      </Carousel.Item>
      <Carousel.Item interval={3000}>
        <div className="d-flex flex-md-row flex-column bd-highlight mb-3">
          <div className="p-2 text-center" style={{ minWidth: '160px' }}>
            <div className="position-relative">
              <Image
                src="/img/caklukman.jpg"
                layout="intrinsic"
                width={100}
                height={100}
                className="rounded-circle mx-auto d-block border"
              />
              <h5>Pak Lukman</h5>
            </div>
          </div>
          <div className="p-2 bg-white mr-auto position-relative fst-italic shadow-sm">

            <FontAwesomeIcon className="fa-2x" icon={['fas', 'quote-left'] as IconProp} />
            <p className="mb-0">Kolam pemancingan BamboeJaya merupakan tempat favorit saya, disamping lokasi dekat dengan rumah saya ikannya juga banyak serta tempatnya sangat nyaman.</p>
          </div>
        </div>
      </Carousel.Item>

    </Carousel>
  );
};

export default Testimonials;
