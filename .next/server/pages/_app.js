/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/_app";
exports.ids = ["pages/_app"];
exports.modules = {

/***/ "./pages/AppContext.tsx":
/*!******************************!*\
  !*** ./pages/AppContext.tsx ***!
  \******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__),\n/* harmony export */   \"useAppProvider\": () => (/* binding */ useAppProvider)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);\n\n// import React from 'react';\n\nconst AppContextDefaultValues = {\n    baseUrl: \"localhost:3000\"\n};\nconst AppContext = /*#__PURE__*/ (0,react__WEBPACK_IMPORTED_MODULE_1__.createContext)(AppContextDefaultValues);\n// export default AppContext;\nfunction useAppProvider() {\n    return (0,react__WEBPACK_IMPORTED_MODULE_1__.useContext)(AppContext);\n}\nconst AppProvider = function({ children  }) {\n    return /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(AppContext.Provider, {\n        value: AppContextDefaultValues,\n        children: children\n    }, void 0, false, {\n        fileName: \"F:\\\\BamboeJaya\\\\web\\\\bamboejaya\\\\pages\\\\AppContext.tsx\",\n        lineNumber: 28,\n        columnNumber: 5\n    }, this);\n};\n// export async function getServerSideProps(context: any) {\n//   return {\n//     props: {\n//       reqUrl: context.req.header.host,\n//     }, // will be passed to the page component as props\n//   };\n// }\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (AppProvider);\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy9BcHBDb250ZXh0LnRzeC5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFDQTtBQURBLDZCQUE2QjtBQUdkO0FBTWYsTUFBTUUsdUJBQXVCLEdBQW1CO0lBQzlDQyxPQUFPLEVBQUUsZ0JBQWdCO0NBQzFCO0FBRUQsTUFBTUMsVUFBVSxpQkFBR0osb0RBQWEsQ0FBaUJFLHVCQUF1QixDQUFDO0FBRXpFLDZCQUE2QjtBQUV0QixTQUFTRyxjQUFjLEdBQUc7SUFDL0IsT0FBT0osaURBQVUsQ0FBQ0csVUFBVSxDQUFDLENBQUM7Q0FDL0I7QUFNRCxNQUFNRSxXQUFXLEdBQUcsU0FBVSxFQUFFQyxRQUFRLEdBQVMsRUFBRTtJQUNqRCxxQkFDRSw4REFBQ0gsVUFBVSxDQUFDSSxRQUFRO1FBQUNDLEtBQUssRUFBRVAsdUJBQXVCO2tCQUNoREssUUFBUTs7Ozs7WUFDVyxDQUN0QjtDQUNIO0FBRUQsMkRBQTJEO0FBQzNELGFBQWE7QUFDYixlQUFlO0FBQ2YseUNBQXlDO0FBQ3pDLDBEQUEwRDtBQUMxRCxPQUFPO0FBQ1AsSUFBSTtBQUVKLGlFQUFlRCxXQUFXLEVBQUMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9iYW1ib2VqYXlhLy4vcGFnZXMvQXBwQ29udGV4dC50c3g/MjM5YyJdLCJzb3VyY2VzQ29udGVudCI6WyIvLyBpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQge1xyXG4gIGNyZWF0ZUNvbnRleHQsIHVzZUNvbnRleHQsIFJlYWN0Tm9kZSxcclxufSBmcm9tICdyZWFjdCc7XHJcblxyXG50eXBlIEFwcENvbnRleHRUeXBlID0ge1xyXG4gIGJhc2VVcmw6IHN0cmluZztcclxufTtcclxuXHJcbmNvbnN0IEFwcENvbnRleHREZWZhdWx0VmFsdWVzOiBBcHBDb250ZXh0VHlwZSA9IHtcclxuICBiYXNlVXJsOiAnbG9jYWxob3N0OjMwMDAnLFxyXG59O1xyXG5cclxuY29uc3QgQXBwQ29udGV4dCA9IGNyZWF0ZUNvbnRleHQ8QXBwQ29udGV4dFR5cGU+KEFwcENvbnRleHREZWZhdWx0VmFsdWVzKTtcclxuXHJcbi8vIGV4cG9ydCBkZWZhdWx0IEFwcENvbnRleHQ7XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gdXNlQXBwUHJvdmlkZXIoKSB7XHJcbiAgcmV0dXJuIHVzZUNvbnRleHQoQXBwQ29udGV4dCk7XHJcbn1cclxuXHJcbnR5cGUgUHJvcHMgPSB7XHJcbiAgY2hpbGRyZW46IFJlYWN0Tm9kZTtcclxufTtcclxuXHJcbmNvbnN0IEFwcFByb3ZpZGVyID0gZnVuY3Rpb24gKHsgY2hpbGRyZW4gfTogUHJvcHMpIHtcclxuICByZXR1cm4gKFxyXG4gICAgPEFwcENvbnRleHQuUHJvdmlkZXIgdmFsdWU9e0FwcENvbnRleHREZWZhdWx0VmFsdWVzfT5cclxuICAgICAge2NoaWxkcmVufVxyXG4gICAgPC9BcHBDb250ZXh0LlByb3ZpZGVyPlxyXG4gICk7XHJcbn07XHJcblxyXG4vLyBleHBvcnQgYXN5bmMgZnVuY3Rpb24gZ2V0U2VydmVyU2lkZVByb3BzKGNvbnRleHQ6IGFueSkge1xyXG4vLyAgIHJldHVybiB7XHJcbi8vICAgICBwcm9wczoge1xyXG4vLyAgICAgICByZXFVcmw6IGNvbnRleHQucmVxLmhlYWRlci5ob3N0LFxyXG4vLyAgICAgfSwgLy8gd2lsbCBiZSBwYXNzZWQgdG8gdGhlIHBhZ2UgY29tcG9uZW50IGFzIHByb3BzXHJcbi8vICAgfTtcclxuLy8gfVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgQXBwUHJvdmlkZXI7XHJcbiJdLCJuYW1lcyI6WyJjcmVhdGVDb250ZXh0IiwidXNlQ29udGV4dCIsIkFwcENvbnRleHREZWZhdWx0VmFsdWVzIiwiYmFzZVVybCIsIkFwcENvbnRleHQiLCJ1c2VBcHBQcm92aWRlciIsIkFwcFByb3ZpZGVyIiwiY2hpbGRyZW4iLCJQcm92aWRlciIsInZhbHVlIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./pages/AppContext.tsx\n");

/***/ }),

/***/ "./pages/_app.tsx":
/*!************************!*\
  !*** ./pages/_app.tsx ***!
  \************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/head */ \"next/head\");\n/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var react_bootstrap_SSRProvider__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap/SSRProvider */ \"react-bootstrap/SSRProvider\");\n/* harmony import */ var react_bootstrap_SSRProvider__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap_SSRProvider__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var bootstrap_dist_css_bootstrap_min_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! bootstrap/dist/css/bootstrap.min.css */ \"./node_modules/bootstrap/dist/css/bootstrap.min.css\");\n/* harmony import */ var bootstrap_dist_css_bootstrap_min_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(bootstrap_dist_css_bootstrap_min_css__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var _styles_globals_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../styles/globals.css */ \"./styles/globals.css\");\n/* harmony import */ var _styles_globals_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_styles_globals_css__WEBPACK_IMPORTED_MODULE_4__);\n/* harmony import */ var _fortawesome_fontawesome_svg_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @fortawesome/fontawesome-svg-core */ \"@fortawesome/fontawesome-svg-core\");\n/* harmony import */ var _fortawesome_fontawesome_svg_core__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_fontawesome_svg_core__WEBPACK_IMPORTED_MODULE_5__);\n/* harmony import */ var _fortawesome_pro_light_svg_icons__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @fortawesome/pro-light-svg-icons */ \"@fortawesome/pro-light-svg-icons\");\n/* harmony import */ var _fortawesome_pro_light_svg_icons__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_pro_light_svg_icons__WEBPACK_IMPORTED_MODULE_6__);\n/* harmony import */ var _fortawesome_pro_solid_svg_icons__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @fortawesome/pro-solid-svg-icons */ \"@fortawesome/pro-solid-svg-icons\");\n/* harmony import */ var _fortawesome_pro_solid_svg_icons__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_pro_solid_svg_icons__WEBPACK_IMPORTED_MODULE_7__);\n/* harmony import */ var _fortawesome_pro_regular_svg_icons__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @fortawesome/pro-regular-svg-icons */ \"@fortawesome/pro-regular-svg-icons\");\n/* harmony import */ var _fortawesome_pro_regular_svg_icons__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_pro_regular_svg_icons__WEBPACK_IMPORTED_MODULE_8__);\n/* harmony import */ var _fortawesome_pro_thin_svg_icons__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @fortawesome/pro-thin-svg-icons */ \"@fortawesome/pro-thin-svg-icons\");\n/* harmony import */ var _fortawesome_pro_thin_svg_icons__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_pro_thin_svg_icons__WEBPACK_IMPORTED_MODULE_9__);\n/* harmony import */ var _fortawesome_pro_duotone_svg_icons__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @fortawesome/pro-duotone-svg-icons */ \"@fortawesome/pro-duotone-svg-icons\");\n/* harmony import */ var _fortawesome_pro_duotone_svg_icons__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_pro_duotone_svg_icons__WEBPACK_IMPORTED_MODULE_10__);\n/* harmony import */ var _fortawesome_free_brands_svg_icons__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @fortawesome/free-brands-svg-icons */ \"@fortawesome/free-brands-svg-icons\");\n/* harmony import */ var _fortawesome_free_brands_svg_icons__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_free_brands_svg_icons__WEBPACK_IMPORTED_MODULE_11__);\n/* harmony import */ var _fortawesome_fontawesome_svg_core_styles_css__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @fortawesome/fontawesome-svg-core/styles.css */ \"./node_modules/@fortawesome/fontawesome-svg-core/styles.css\");\n/* harmony import */ var _fortawesome_fontawesome_svg_core_styles_css__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_fontawesome_svg_core_styles_css__WEBPACK_IMPORTED_MODULE_12__);\n/* harmony import */ var _AppContext__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./AppContext */ \"./pages/AppContext.tsx\");\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n_fortawesome_fontawesome_svg_core__WEBPACK_IMPORTED_MODULE_5__.config.autoAddCss = false;\n_fortawesome_fontawesome_svg_core__WEBPACK_IMPORTED_MODULE_5__.library.add(_fortawesome_pro_light_svg_icons__WEBPACK_IMPORTED_MODULE_6__.fal, _fortawesome_pro_solid_svg_icons__WEBPACK_IMPORTED_MODULE_7__.fas, _fortawesome_pro_regular_svg_icons__WEBPACK_IMPORTED_MODULE_8__.far, _fortawesome_pro_thin_svg_icons__WEBPACK_IMPORTED_MODULE_9__.fat, _fortawesome_pro_duotone_svg_icons__WEBPACK_IMPORTED_MODULE_10__.fad, _fortawesome_free_brands_svg_icons__WEBPACK_IMPORTED_MODULE_11__.fab);\nconst MyApp = function({ Component , pageProps  }) {\n    return /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_AppContext__WEBPACK_IMPORTED_MODULE_13__[\"default\"], {\n        children: /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((react_bootstrap_SSRProvider__WEBPACK_IMPORTED_MODULE_2___default()), {\n            children: [\n                /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((next_head__WEBPACK_IMPORTED_MODULE_1___default()), {\n                    children: /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"meta\", {\n                        name: \"viewport\",\n                        content: \"width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=0\"\n                    }, void 0, false, {\n                        fileName: \"F:\\\\BamboeJaya\\\\web\\\\bamboejaya\\\\pages\\\\_app.tsx\",\n                        lineNumber: 27,\n                        columnNumber: 11\n                    }, this)\n                }, void 0, false, {\n                    fileName: \"F:\\\\BamboeJaya\\\\web\\\\bamboejaya\\\\pages\\\\_app.tsx\",\n                    lineNumber: 26,\n                    columnNumber: 9\n                }, this),\n                /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(Component, {\n                    ...pageProps\n                }, void 0, false, {\n                    fileName: \"F:\\\\BamboeJaya\\\\web\\\\bamboejaya\\\\pages\\\\_app.tsx\",\n                    lineNumber: 29,\n                    columnNumber: 9\n                }, this)\n            ]\n        }, void 0, true, {\n            fileName: \"F:\\\\BamboeJaya\\\\web\\\\bamboejaya\\\\pages\\\\_app.tsx\",\n            lineNumber: 25,\n            columnNumber: 7\n        }, this)\n    }, void 0, false, {\n        fileName: \"F:\\\\BamboeJaya\\\\web\\\\bamboejaya\\\\pages\\\\_app.tsx\",\n        lineNumber: 24,\n        columnNumber: 5\n    }, this);\n};\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (MyApp);\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy9fYXBwLnRzeC5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDNkI7QUFFeUI7QUFDUjtBQUNmO0FBRXFDO0FBQ2I7QUFDQTtBQUNFO0FBQ0g7QUFDRztBQUNBO0FBQ0g7QUFFZjtBQUV2Q0csZ0ZBQWlCLEdBQUcsS0FBSyxDQUFDO0FBQzFCRCwwRUFBVyxDQUFDRSxpRUFBRyxFQUFFQyxpRUFBRyxFQUFFQyxtRUFBRyxFQUFFQyxnRUFBRyxFQUFFQyxvRUFBRyxFQUFFQyxvRUFBRyxDQUFDLENBQUM7QUFFMUMsTUFBTUksS0FBSyxHQUFHLFNBQVUsRUFBRUMsU0FBUyxHQUFFQyxTQUFTLEdBQVksRUFBRTtJQUMxRCxxQkFDRSw4REFBQ0wsb0RBQVc7a0JBQ1YsNEVBQUNULG9FQUFXOzs4QkFDViw4REFBQ0Qsa0RBQUk7OEJBQ0gsNEVBQUNnQixNQUFJO3dCQUFDQyxJQUFJLEVBQUMsVUFBVTt3QkFBQ0MsT0FBTyxFQUFDLDBFQUEwRTs7Ozs7NEJBQUc7Ozs7O3dCQUN0Rzs4QkFDUCw4REFBQ0osU0FBUztvQkFBRSxHQUFHQyxTQUFTOzs7Ozt3QkFBSTs7Ozs7O2dCQUNoQjs7Ozs7WUFDRixDQUNkO0NBQ0g7QUFDRCxpRUFBZUYsS0FBSyxFQUFDIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vYmFtYm9lamF5YS8uL3BhZ2VzL19hcHAudHN4PzJmYmUiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHR5cGUgeyBBcHBQcm9wcyB9IGZyb20gJ25leHQvYXBwJztcbmltcG9ydCBIZWFkIGZyb20gJ25leHQvaGVhZCc7XG5cbmltcG9ydCBTU1JQcm92aWRlciBmcm9tICdyZWFjdC1ib290c3RyYXAvU1NSUHJvdmlkZXInO1xuaW1wb3J0ICdib290c3RyYXAvZGlzdC9jc3MvYm9vdHN0cmFwLm1pbi5jc3MnO1xuaW1wb3J0ICcuLi9zdHlsZXMvZ2xvYmFscy5jc3MnO1xuXG5pbXBvcnQgeyBsaWJyYXJ5LCBjb25maWcgfSBmcm9tICdAZm9ydGF3ZXNvbWUvZm9udGF3ZXNvbWUtc3ZnLWNvcmUnO1xuaW1wb3J0IHsgZmFsIH0gZnJvbSAnQGZvcnRhd2Vzb21lL3Byby1saWdodC1zdmctaWNvbnMnO1xuaW1wb3J0IHsgZmFzIH0gZnJvbSAnQGZvcnRhd2Vzb21lL3Byby1zb2xpZC1zdmctaWNvbnMnO1xuaW1wb3J0IHsgZmFyIH0gZnJvbSAnQGZvcnRhd2Vzb21lL3Byby1yZWd1bGFyLXN2Zy1pY29ucyc7XG5pbXBvcnQgeyBmYXQgfSBmcm9tICdAZm9ydGF3ZXNvbWUvcHJvLXRoaW4tc3ZnLWljb25zJztcbmltcG9ydCB7IGZhZCB9IGZyb20gJ0Bmb3J0YXdlc29tZS9wcm8tZHVvdG9uZS1zdmctaWNvbnMnO1xuaW1wb3J0IHsgZmFiIH0gZnJvbSAnQGZvcnRhd2Vzb21lL2ZyZWUtYnJhbmRzLXN2Zy1pY29ucyc7XG5pbXBvcnQgJ0Bmb3J0YXdlc29tZS9mb250YXdlc29tZS1zdmctY29yZS9zdHlsZXMuY3NzJztcblxuaW1wb3J0IEFwcFByb3ZpZGVyIGZyb20gJy4vQXBwQ29udGV4dCc7XG5cbmNvbmZpZy5hdXRvQWRkQ3NzID0gZmFsc2U7XG5saWJyYXJ5LmFkZChmYWwsIGZhcywgZmFyLCBmYXQsIGZhZCwgZmFiKTtcblxuY29uc3QgTXlBcHAgPSBmdW5jdGlvbiAoeyBDb21wb25lbnQsIHBhZ2VQcm9wcyB9OiBBcHBQcm9wcykge1xuICByZXR1cm4gKFxuICAgIDxBcHBQcm92aWRlcj5cbiAgICAgIDxTU1JQcm92aWRlcj5cbiAgICAgICAgPEhlYWQ+XG4gICAgICAgICAgPG1ldGEgbmFtZT1cInZpZXdwb3J0XCIgY29udGVudD1cIndpZHRoPWRldmljZS13aWR0aCwgaW5pdGlhbC1zY2FsZT0xLjAsIG1heGltdW0tc2NhbGU9MS4wLHVzZXItc2NhbGFibGU9MFwiIC8+XG4gICAgICAgIDwvSGVhZD5cbiAgICAgICAgPENvbXBvbmVudCB7Li4ucGFnZVByb3BzfSAvPlxuICAgICAgPC9TU1JQcm92aWRlcj5cbiAgICA8L0FwcFByb3ZpZGVyPlxuICApO1xufTtcbmV4cG9ydCBkZWZhdWx0IE15QXBwO1xuIl0sIm5hbWVzIjpbIkhlYWQiLCJTU1JQcm92aWRlciIsImxpYnJhcnkiLCJjb25maWciLCJmYWwiLCJmYXMiLCJmYXIiLCJmYXQiLCJmYWQiLCJmYWIiLCJBcHBQcm92aWRlciIsImF1dG9BZGRDc3MiLCJhZGQiLCJNeUFwcCIsIkNvbXBvbmVudCIsInBhZ2VQcm9wcyIsIm1ldGEiLCJuYW1lIiwiY29udGVudCJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./pages/_app.tsx\n");

/***/ }),

/***/ "./node_modules/@fortawesome/fontawesome-svg-core/styles.css":
/*!*******************************************************************!*\
  !*** ./node_modules/@fortawesome/fontawesome-svg-core/styles.css ***!
  \*******************************************************************/
/***/ (() => {



/***/ }),

/***/ "./node_modules/bootstrap/dist/css/bootstrap.min.css":
/*!***********************************************************!*\
  !*** ./node_modules/bootstrap/dist/css/bootstrap.min.css ***!
  \***********************************************************/
/***/ (() => {



/***/ }),

/***/ "./styles/globals.css":
/*!****************************!*\
  !*** ./styles/globals.css ***!
  \****************************/
/***/ (() => {



/***/ }),

/***/ "@fortawesome/fontawesome-svg-core":
/*!****************************************************!*\
  !*** external "@fortawesome/fontawesome-svg-core" ***!
  \****************************************************/
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/fontawesome-svg-core");

/***/ }),

/***/ "@fortawesome/free-brands-svg-icons":
/*!*****************************************************!*\
  !*** external "@fortawesome/free-brands-svg-icons" ***!
  \*****************************************************/
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/free-brands-svg-icons");

/***/ }),

/***/ "@fortawesome/pro-duotone-svg-icons":
/*!*****************************************************!*\
  !*** external "@fortawesome/pro-duotone-svg-icons" ***!
  \*****************************************************/
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/pro-duotone-svg-icons");

/***/ }),

/***/ "@fortawesome/pro-light-svg-icons":
/*!***************************************************!*\
  !*** external "@fortawesome/pro-light-svg-icons" ***!
  \***************************************************/
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/pro-light-svg-icons");

/***/ }),

/***/ "@fortawesome/pro-regular-svg-icons":
/*!*****************************************************!*\
  !*** external "@fortawesome/pro-regular-svg-icons" ***!
  \*****************************************************/
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/pro-regular-svg-icons");

/***/ }),

/***/ "@fortawesome/pro-solid-svg-icons":
/*!***************************************************!*\
  !*** external "@fortawesome/pro-solid-svg-icons" ***!
  \***************************************************/
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/pro-solid-svg-icons");

/***/ }),

/***/ "@fortawesome/pro-thin-svg-icons":
/*!**************************************************!*\
  !*** external "@fortawesome/pro-thin-svg-icons" ***!
  \**************************************************/
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/pro-thin-svg-icons");

/***/ }),

/***/ "next/head":
/*!****************************!*\
  !*** external "next/head" ***!
  \****************************/
/***/ ((module) => {

"use strict";
module.exports = require("next/head");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/***/ ((module) => {

"use strict";
module.exports = require("react");

/***/ }),

/***/ "react-bootstrap/SSRProvider":
/*!**********************************************!*\
  !*** external "react-bootstrap/SSRProvider" ***!
  \**********************************************/
/***/ ((module) => {

"use strict";
module.exports = require("react-bootstrap/SSRProvider");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ ((module) => {

"use strict";
module.exports = require("react/jsx-dev-runtime");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("./pages/_app.tsx"));
module.exports = __webpack_exports__;

})();