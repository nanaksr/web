import type { AppProps } from 'next/app';
import Head from 'next/head';

import SSRProvider from 'react-bootstrap/SSRProvider';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/globals.css';

import { library, config } from '@fortawesome/fontawesome-svg-core';
import { fal } from '@fortawesome/pro-light-svg-icons';
import { fas } from '@fortawesome/pro-solid-svg-icons';
import { far } from '@fortawesome/pro-regular-svg-icons';
import { fat } from '@fortawesome/pro-thin-svg-icons';
import { fad } from '@fortawesome/pro-duotone-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
import '@fortawesome/fontawesome-svg-core/styles.css';

import AppProvider from './AppContext';

config.autoAddCss = false;
library.add(fal, fas, far, fat, fad, fab);

const MyApp = function ({ Component, pageProps }: AppProps) {
  return (
    <AppProvider>
      <SSRProvider>
        <Head>
          <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=0" />
        </Head>
        <Component {...pageProps} />
      </SSRProvider>
    </AppProvider>
  );
};
export default MyApp;
