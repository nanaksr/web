// import React from 'react';
import {
  createContext, useContext, ReactNode,
} from 'react';

type AppContextType = {
  baseUrl: string;
};

const AppContextDefaultValues: AppContextType = {
  baseUrl: 'localhost:3000',
};

const AppContext = createContext<AppContextType>(AppContextDefaultValues);

// export default AppContext;

export function useAppProvider() {
  return useContext(AppContext);
}

type Props = {
  children: ReactNode;
};

const AppProvider = function ({ children }: Props) {
  return (
    <AppContext.Provider value={AppContextDefaultValues}>
      {children}
    </AppContext.Provider>
  );
};

// export async function getServerSideProps(context: any) {
//   return {
//     props: {
//       reqUrl: context.req.header.host,
//     }, // will be passed to the page component as props
//   };
// }

export default AppProvider;
