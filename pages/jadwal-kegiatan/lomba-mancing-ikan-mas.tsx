import React from 'react';

// import Head from 'next/head';
import Link from 'next/link';
// import Image from 'next/image';

import {
  Container, Row, Col, Button,
} from 'react-bootstrap';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import MainLayout from '../../components/MainLayout';

const jadwalkegiatan = function () {
  return (
    <MainLayout
      pageTitle="Lomba Mancing Rilis Ikan Lele"
      metaKeywords="rilis ikan lele, lomba ikan lele, lomba mancing ikan lele, lomba mancing rilis ikan lele"
      metaDescription="Ikutilah lomba mancing rilis ikan lele di kolam pemancingan BamboeJaya Malang"
      metaImage={`${process.env.BASE_URL}/img/card-rilis-lele.jpg`}
    >
      <header className="bgMain">
        <div className="headcontentWrapper">
          <Container>
            <Row className="justify-content-center">
              <Col md="7">
                <h6 className="text-center text-muted mb-0">LOMBA MANCING</h6>
                <h1>IKAN MAS</h1>
                <p>Tunjukan skil memancing anda dan raih kesempatan untuk mendapatkan hadiah dalam setiap perlombaanya</p>
              </Col>
            </Row>
          </Container>
        </div>
      </header>

      <Container style={{ paddingTop: '37px' }}>
        <p className="lead text-wrap">Lomba mancing ikan mas merupakan kegiatan lomba memancing yang bertujuan untuk berkompetisi antar pemancing. Skil, teknik, serta keahlian dalam meracik umpan adalah kunci keberhasilan dalam meraih kemenangan. Lomba ini menggunakan sistem tebar bawa pulang, yaitu ikan yang sudah terangkat dapat dibawa pulang. Seluruh pemancing berlomba lomba untuk mendapatkan ikan yang terberat agar dapat memenangkan kompetisi ini.</p>
        <Row className="justify-content-md-center mb-5">
          <Col md="5">
            <h1 className="text-center mb-0 mt-lg-4">TIKET UTAMA 60000</h1>
            <h5 className="text-center mb-4">PLUS 2 X C @10000 (TIDAK WAJIB)</h5>
            <h6 className="text-warning text-center my-1">
              <FontAwesomeIcon icon={['fal', 'calendar-clock'] as IconProp} />
              {' '}
              DURASI LOMBA 2 JAM 30 MENIT
            </h6>
            <h6 className="text-primary text-center my-1">
              <FontAwesomeIcon icon={['fal', 'person-seat'] as IconProp} />
              {' '}
              SISTEM LAPAK BEBAS
            </h6>
            <h4 className="text-center fst-italic mt-4">5 NOMINASI HADIAH</h4>
          </Col>
          <Col md="4">
            <div className="position-relative bg-white shadow p-3" style={{ borderRadius: '6px' }}>
              <div className="d-flex justify-content-between mb-3 border-bottom">
                <h5 className="text-start">JUARA 1</h5>
                <h5 className="text-end">
                  <sup>Rp</sup>
                  600.000
                </h5>
              </div>
              <div className="d-flex justify-content-between mb-3 border-bottom">
                <h5 className="text-start">JUARA 2</h5>
                <h5 className="text-end">
                  <sup>Rp</sup>
                  100.000
                </h5>
              </div>
              <div className="d-flex justify-content-between mb-3 border-bottom">
                <h5 className="text-start">JUARA 3</h5>
                <h5 className="text-end">
                  <sup>Rp</sup>
                  80.000
                </h5>
              </div>
              <div className="d-flex justify-content-between mb-3 border-bottom">
                <h5 className="text-start">JUARA 4</h5>
                <h5 className="text-end">
                  <sup>Rp</sup>
                  70.000
                </h5>
              </div>
              <div className="d-flex justify-content-between mb-3 border-bottom">
                <h5 className="text-start">JUARA 5</h5>
                <h5 className="text-end">
                  <sup>Rp</sup>
                  60.000
                </h5>
              </div>
            </div>
          </Col>
        </Row>

        <Row>
          <Col>
            <h6>Peraturan Lomba</h6>
            <ol>
              <li>MAKSIMAL 2 MATA KAIL</li>
              <li>UMPAN LARANGAN : CACING DAN LUMUT</li>
              <li>MAKSIMAL GANDENG BEBAS</li>
            </ol>
          </Col>
        </Row>

        <Row>
          <div className="position-relative bg-primary p-3" style={{ borderRadius: '6px' }}>
            <p className="fs-2 text-center mb-0 text-white">Lomba diadakan setiap hari Senin pukul 20:00 WIB</p>
          </div>
        </Row>
      </Container>

      <h5 className="text-center mt-5 text-capitalize">hubungi kami kapan saja untuk mendapatkan informasi lebih lanjut</h5>
      <div className="text-center mt-3 mb-5">
        <Link href="/kontak">
          <Button variant="info" size="lg" className="text-white">
            <FontAwesomeIcon icon={['fas', 'user-headset'] as IconProp} />
            {' '}
            &nbsp;
            Kontak Kami
          </Button>
        </Link>
      </div>

    </MainLayout>
  );
};

export default jadwalkegiatan;
