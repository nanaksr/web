import React from 'react';

// import Head from 'next/head';
import Link from 'next/link';
import Image from 'next/image';

import {
  Container, Row, Col, Button,
} from 'react-bootstrap';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import MainLayout from '../../components/MainLayout';

const jadwalkegiatan = function () {
  return (
    <MainLayout
      pageTitle="Jadwal Kegiatan BamboeJaya"
      metaKeywords="jadwal mancing, jadwal lomba mancing, rilis lele, rilis lele kota malang, mancing karangploso, mancing tombro, lomba ikan mas, lomba ikan tombro"
      metaDescription="Jadwal kegiatan kolam pemancingan BamboeJaya"
    >
      <header className="bgMain">
        <div className="headcontentWrapper">
          <Container>
            <Row className="justify-content-center">
              <Col md="7">
                <h1>Jadwal Dan Kegiatan</h1>
                <p>Berbagai macam kegiatan serta acara yang kami suguhkan bagi anda</p>
              </Col>
            </Row>
          </Container>
        </div>
      </header>

      <Container style={{ paddingTop: '77px' }}>
        <Row>
          <Col md="6" className="mb-5">
            <div className="position-relative bg-light" style={{ borderRadius: '8px', padding: '105px 20px 20px 20px' }}>
              <div className="position-absolute" style={{ top: '-30px', left: '10px' }}>
                <Image
                  src="/img/ikanlele.png"
                  layout="intrinsic"
                  width={195}
                  height={119}
                />
              </div>
              <h4 className="mb-2">Lomba Mancing Rilis Ikan Lele</h4>
              <p className="lead lh-sm">Kegiatan lomba rutin Kolam Pemancingan BamboeJaya, raih kesempatan untuk mendapatkan hadiah utama sebesar Rp. 1.500.000 serta total hadiah keseluruhan lebih dari Rp. 2.000.000 pada setiap kegiatannya.</p>

              <Link href="/jadwal-kegiatan/lomba-mancing-rilis-ikan-lele">
                <a role="button" className="btn btn-outline-success btn-sm">
                  Lebih Lanjut
                  {' '}
                  <FontAwesomeIcon icon={['fas', 'arrow-right'] as IconProp} />
                </a>
              </Link>

              {/*
              <p className="lh-sm">Lomba mancing rilis ikan lele merupakan kegiatan lomba memancing yang bertujuan untuk berkompetisi antar pemancing. Skil, teknik, serta keahlian dalam meracik umpan adalah kunci keberhasilan dalam meraih kemenangan. Lomba ini menggunakan sistem rilis yaitu ikan yang telah di dapat akan di rilis atau dikembalikan lagi kedalam kolam. Seluruh pemancing berlomba lomba untuk mendapatkan ikan yang terberat agar dapat memenangkan kompetisi ini.</p>
              <p className="lh-sm">Jadwal lomba diadakan setiap Hari Selasa dan Sabtu pukul 21:00 WIB (jam 9 malam)</p>
              <p className="lh-sm">Raih kesempatan untuk mendapatkan total Hadiah lebih dari 2 JUTA rupiah pada setiap perlombannya</p>
              */}

            </div>
          </Col>
          <Col md="6" className="mb-5">
            <div className="position-relative bg-light" style={{ borderRadius: '8px', padding: '105px  20px 20px 20px' }}>
              <div className="position-absolute" style={{ top: '-30px', left: '10px' }}>
                <Image
                  src="/img/ikanmas.png"
                  layout="intrinsic"
                  width={195}
                  height={129}
                />
              </div>
              <h4 className="mb-2">Lomba Mancing Ikan Mas</h4>
              <p className="lead lh-sm">Kegiatan lomba rutin yang ditunggu - tunggu bagi para pecinta mancing ikan mas / tombro. Raih kesempatan untuk memenangkan perlombaan serta dapatkan hadiah menarik yang telah kami sediakan untuk anda.</p>
              <Link href="/jadwal-kegiatan/lomba-mancing-ikan-mas">
                <a role="button" className="btn btn-outline-success btn-sm">
                  Lebih Lanjut
                  {' '}
                  <FontAwesomeIcon icon={['fas', 'arrow-right'] as IconProp} />
                </a>
              </Link>
              {/*
              <Link href="/">
                <a role="button" className="fw-bold">
                  Lebih Lanjut
                  {' '}
                  <FontAwesomeIcon icon={['fas', 'arrow-right'] as IconProp} />
                </a>
              </Link>
              */}
            </div>
          </Col>
        </Row>
      </Container>

      <Container>
        <div className="bg-warning bg-gradient text-white p-4" style={{ borderRadius: '8px' }}>
          <Row>
            <Col md="3" className="text-center">
              <Image
                src="/img/harian_jadwal.png"
                layout="intrinsic"
                width={191}
                height={138}
              />
            </Col>
            <Col md="9">
              <h4>Mancing Harian Santai</h4>
              <p className="mt-3">Mancing harian santai buka setiap hari mulai dari pukul 8 Pagi hingga 12 Malam.</p>
              <p>Isi waktu luang  dan ajak orang - orang terdekat anda untuk memancing dan menikmati ketenangan suasa sekitar kolam.</p>
            </Col>

          </Row>
        </div>
      </Container>

      <h5 className="text-center mt-5 text-capitalize">hubungi kami kapan saja untuk mendapatkan informasi lebih lanjut</h5>
      <div className="text-center mt-3 mb-5">
        <Link href="/kontak">
          <Button variant="info" size="lg" className="text-white">
            <FontAwesomeIcon icon={['fas', 'user-headset'] as IconProp} />
            {' '}
            &nbsp;
            Kontak Kami
          </Button>
        </Link>
      </div>

    </MainLayout>
  );
};

export default jadwalkegiatan;
