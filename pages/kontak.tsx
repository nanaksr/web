import React from 'react';

// import Head from 'next/head';
// import Link from 'next/link';
// import Image from 'next/image';

import {
  Container, Row, Col, Card,
} from 'react-bootstrap';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import MainLayout from '../components/MainLayout';

const jadwalkegiatan = function () {
  return (
    <MainLayout
      pageTitle="Kontak BmaboeJaya"
      metaKeywords="kolam pemancingan bamboejaya,ayo mancing, rilis lele di kota malang, kontak support mancing, daftar mancing, daftar lomba mancing"
      metaDescription="Hubungi kami kapan saja untuk mendapatkan informasi mengenai kolam pemancingan BamboeJaya"
    >
      <header className="bgMain">
        <div className="headcontentWrapper">
          <Container>
            <Row className="justify-content-center">
              <Col md="7">
                <h1>Kontak Kami</h1>
                <p>Hubungi Kami Kapan Saja Untuk Informasi Dan Pendaftaran Lomba</p>
              </Col>
            </Row>
          </Container>
        </div>
      </header>

      <Container style={{ paddingTop: '77px' }}>
        <Row className="mb-4">
          <Col md={3}>
            <p className="fs-5">Kami siap menjawab dan memberikan informasi apa saja mengenai Kolam Pemancingan BamboeJaya</p>
          </Col>
          <Col md={5}>
            <Card className="rounded border-0 hover-top py-1 px-3 shadow" style={{ minHeight: '215px' }}>
              <Card.Body>
                <FontAwesomeIcon icon={['fat', 'map-location-dot'] as IconProp} className="fa-4x mb-1" />
                <h5>Alamat Kami</h5>
                <p>JL. Tamanu diharjo, Mbunder Ledok, Ampeldento, Karangploso, Kabupaten Malang</p>
              </Card.Body>
            </Card>
          </Col>
          <Col md={4}>
            <Card className="rounded border-0 hover-top py-1 px-3 shadow" style={{ minHeight: '215px' }}>
              <Card.Body>
                <FontAwesomeIcon icon={['fat', 'user-headset'] as IconProp} className="fa-4x mb-1" />
                <h5>Hubungi Kami</h5>
                <p className="fs-4 mb-0">082-331-761-443</p>
                <small>Telepon, SMS, WhatsApp</small>
              </Card.Body>
            </Card>
          </Col>
        </Row>

        <Row>
          <Col>
            <div className="google-map-code">
              <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3951.84781232991!2d112.59324831530897!3d-7.910962080930223!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e788192cf2269d9%3A0xa377fd30f432b5f6!2sKolam%20Pemancingan%20Bamboe%20Jaya!5e0!3m2!1sen!2sid!4v1655301558229!5m2!1sen!2sid"
                allowFullScreen
                loading="lazy"
                title="maps bamboe"
                style={{ width: '100%', height: '450px' }}
              />
            </div>
          </Col>
        </Row>

      </Container>

    </MainLayout>
  );
};

export default jadwalkegiatan;
