import React from 'react';

// import Head from 'next/head';
import Image from 'next/image';

import {
  Container, Row, Col, Card,
} from 'react-bootstrap';

import MainLayout from '../components/MainLayout';

import Testimonials from '../components/Testimonials';

const tentangkami = function () {
  return (
    <MainLayout
      pageTitle="Tentang BamboeJaya"
      metaKeywords="kp bamboe jaya, bamboejaya, kolam mancing malang, kolam pemancingan terbaik, kolam di malang, lomba mancing malang"
      metaDescription="Mengenal lebih dekat kolam pemancingan BamboeJaya"
    >
      <header className="bgMain">
        <div className="headcontentWrapper">
          <Container>
            <Row className="justify-content-center">
              <Col md="7">
                <h1>Tentang Kami</h1>
                <p>Lebih dari 5 tahun kami hadir sebagai sarana penyalur hobi memancing anda. Kami akan terus berbenah dan memberikan yang terbaik</p>
              </Col>
            </Row>
          </Container>
        </div>
      </header>

      <Container className="mt-5">

        <Row className="mb-5">
          <Col md="5" className="mb-3 text-center">
            <Image
              src="/img/about2.png"
              layout="intrinsic"
              width={465}
              height={545}
            />
          </Col>
          <Col md="7" className="mb-3">
            <h6 className="text-info fw-bold">Tentang Kami</h6>
            <h2>Kolam Pemancingan BamboeJaya</h2>
            <p>Kolam pemancingan Bamboe Jaya terletak di area yang nyaman dan asri, dimana pada sekeliling terdapat pepohonan rindang serta udara sejuk. Sudah banyak pemancing datang dan membuktikannya. Fasilitas utama seperti toilet dan kantin yang kami sediakan agar menambah kenyamanan anda saat memancing. Kami hadir sebagai sarana menyalurkan hobi memancing dan berkompetisi dalam dunia memancing anda. Kami berdiri sejak tahun 2016 hingga sekarang, sudah ratusan bahkan ribuan pemancing di seluruh Malang Raya yang telah menyalurkan hobi memancing mereka di Kolam Pemancingan BamboeJaya.</p>
            <p>Kolam Pemancingan BamboeJaya memiliki dua kolam yang cukup luas dan masing - masing kolam memiliki fungsi yang berbeda, kolam 1 digunakan untuk Lomba Rilis LELE dan Kolam 2 digunakan untuk lomba Ikan Tombro serta mancing harian. Kolam Pemancingan BamboeJaya buka setiap hari untuk mancing harian dan untuk lomba 4 kali dalam seminggu.</p>
          </Col>
        </Row>
      </Container>

      <section className="py-5 bg-light">
        <Container>
          <Row>
            <h2 className="text-center mb-5">Fasilitas dan Layanan</h2>
            <Col md="4" className="mb-3">
              <Card className="p-2 border-0" style={{ borderRadius: '10px', minHeight: '5em' }}>
                <Card.Body className="text-center">
                  <Image
                    src="/img/about-kolam.png"
                    layout="intrinsic"
                    width={200}
                    height={200}
                  />
                  <div className="text-start">
                    <p className="fs-6 fw-bold mb-1">Kolam Pancing</p>
                    <p className="lh-sm">Terdapat dua kolam yang tersedia sebagai fasilitas utama kami.</p>
                  </div>
                </Card.Body>
              </Card>
            </Col>
            <Col md="4" className="mb-3">
              <Card className="p-2 border-0" style={{ borderRadius: '10px' }}>
                <Card.Body className="text-center">
                  <Image
                    src="/img/about-kantin.png"
                    layout="intrinsic"
                    width={200}
                    height={200}
                  />
                  <div className="text-start">
                    <p className="fs-6 fw-bold mb-1">Kantin</p>
                    <p className="lh-sm">Tersedia kantin yang menjual berbagai macam makan, minuman, serta umpan.</p>
                  </div>
                </Card.Body>
              </Card>
            </Col>
            <Col md="4" className="mb-3">
              <Card className="p-2 border-0" style={{ borderRadius: '10px' }}>
                <Card.Body className="text-center">
                  <Image
                    src="/img/about-toilet.png"
                    layout="intrinsic"
                    width={200}
                    height={200}
                  />
                  <div className="text-start">
                    <p className="fs-6 fw-bold mb-1">Toilet</p>
                    <p className="lh-sm">Toilet air bersih tersedia untuk menambah kenyamanan anda.</p>
                  </div>
                </Card.Body>
              </Card>
            </Col>
          </Row>

        </Container>
      </section>

      <Container className="py-5">
        <h2 className="text-center mb-3">Apa kata Mereka</h2>
        <Row>
          <Col md="6"><p className="fs-5">Berikut ulasan dari beberapa pemancing yang telah datang ke kolam kami</p></Col>
          <Col md="6">
            <Testimonials />
          </Col>
        </Row>
      </Container>

    </MainLayout>
  );
};

export default tentangkami;
