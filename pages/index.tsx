import dynamic from 'next/dynamic';
import React from 'react';

import Image from 'next/image';
import Link from 'next/link';

import {
  Container, Row, Col,
} from 'react-bootstrap';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import styles from '../styles/Home.module.css';
import MainLayout from '../components/MainLayout';

const Testimonials = dynamic(() => import('../components/Testimonials'));

const Home = function () {
  return (
    <MainLayout
      pageTitle="Kolam Pemancingan BamboeJaya"
      metaKeywords="kolam pemancingan, kolam mancing, kolam pemancingan di kota malang, kolam lomba, lomba mancing"
      metaDescription="Kolam pemancingan BamboeJaya mengadakan kegiatan mancing Harian, Lomba, dan Galatama pada setiap harinya. Datang dan ikuti setiap kegiatan kami serta raih kesempatan untuk mendapatkan hadiahnya."
      metaImage={`${process.env.BASE_URL}/img/social-card.jpg`}
    >
      <header className={styles.bghome}>
        <div className="headcontentWrapper">
          <Container>
            <Row className="justify-content-center text-center">
              <Col md="7">
                <h1>
                  Selamat Datang
                  {' '}
                  <br />
                  Di Kolam Pemancingan
                </h1>
                <Image
                  src="/img/logo-bordered.png"
                  layout="intrinsic"
                  width={210}
                  height={160}
                />
              </Col>
            </Row>
          </Container>
        </div>
      </header>

      <Container id="tentangkami" className="py-5">
        <Row className="justify-content-md-center py-4">
          <Col md="6" className="text-center">
            <div className="position-relative">
              <div className="position-absolute top-0 start-0 z-index-0">
                <Image
                  src="/img/bg-pattern.png"
                  layout="intrinsic"
                  width={172}
                  height={172}
                />
              </div>
              <div className="position-absolute bottom-0 end-0 z-index-0">
                <Image
                  src="/img/bg-pattern.png"
                  layout="intrinsic"
                  width={172}
                  height={172}
                />
              </div>
              <div className="position-relative z-index-1 py-3">
                <Image
                  src="/img/profilehome.jpg"
                  layout="intrinsic"
                  width={480}
                  height={445}
                  className="rounded"
                />
              </div>

            </div>
          </Col>
          <Col md="6" className="my-3">
            <h6 className="text-info fw-bold mt-4">Tentang Kami</h6>
            <div className="pb-3">
              <h3 className="mb-1-6">Kolam Pemancingan Bamboe Jaya</h3>
              <span className="divider m-0 bg-success mb-1" />
            </div>
            <p className="lead">Kolam pemancingan Bamboe Jaya terletak di area yang nyaman dan asri, dimana pada sekeliling terdapat pepohonan rindang serta udara sejuk. Sudah banyak pemancing datang dan membuktikannya. Fasilitas utama seperti toilet dan kantin yang kami sediakan agar menambah kenyamanan anda saat memancing. Kami hadir sebagai sarana menyalurkan hobi memancing dan berkompetisi dalam dunia memancing anda.</p>
            <Link href="/tentang-kami">
              <a className="btn btn-success py-2" style={{ fontWeight: '600' }}>
                SELENGKAPNYA
                {' '}
                <FontAwesomeIcon icon={['fad', 'arrow-circle-right'] as IconProp} />
              </a>
            </Link>
          </Col>
        </Row>
      </Container>

      <section className="py-5 bg-light mb-5" id="jadwalkegiatan">
        <Container>
          <div className="text-center mt-3 mb-5">
            <h1>Jadwal dan Kegiatan</h1>
            <span className="divider bg-info mx-auto" />
          </div>

          <Row className="justify-content-center mb-5">
            <Col md="6" className="text-center">
              <h6 className="text-secondary text-opacity-50 d-block d-sm-none">Lomba Mancing</h6>
              <h2 className="d-block d-sm-none mb-3">Spesial Rilis Ikan LELE</h2>
              <Image
                src="/img/jadwalrilis.png"
                layout="intrinsic"
                width={525}
                height={360}
                className="rounded"
              />
            </Col>
            <Col md="6" className="text-md-start text-center">
              <h6 className="text-secondary text-opacity-50 mt-4 d-none d-md-block">Lomba Mancing</h6>
              <h2 className="d-none d-md-block">Spesial Rilis Ikan LELE</h2>
              <p className="pe-3">Lomba atau Galatama rutin Spesial Rilis LELE Kolam Pemancingan BamboeJaya diadakan 2 kali dalam seminggu dengan memperebutkan Hadiah Total lebih dari Rp. 2.000.000</p>
              <Link href="/jadwal-kegiatan">
                <a className="btn btn-success py-2 mt-md-4" style={{ fontWeight: '600' }}>
                  CEK SELENGKAPNYA
                  {' '}
                  <FontAwesomeIcon icon={['fad', 'arrow-circle-right'] as IconProp} />
                </a>
              </Link>
            </Col>
          </Row>

          <Row className="justify-content-center mb-5">
            <Col md="6" className="text-center order-1 order-md-2">
              <h6 className="text-secondary text-opacity-50 d-block d-sm-none">Lomba Mancing</h6>
              <h2 className="d-block d-sm-none">Ikan Tombro Tebar</h2>
              <Image
                src="/img/jadwaltombrohome.png"
                layout="intrinsic"
                width={525}
                height={360}
                className="rounded "
              />
            </Col>
            <Col md="6" className="text-md-start text-center order-2 order-md-1">
              <h6 className="text-secondary text-opacity-50 mt-4 d-none d-md-block">Lomba Mancing</h6>
              <h2 className="d-none d-md-block">Ikan Tombro Tebar</h2>
              <p className="pe-3">Lomba mancing ikan Mas / Tombro tebar ikan baru dan ikan yang diperoleh dapat dibawa pulang, diadakan 2 kali dalam seminggu setiap hari Senin dan Kamis disetiap mingguanya.</p>
              <Link href="/jadwal-kegiatan">
                <a className="btn btn-success py-2 mt-md-4" style={{ fontWeight: '600' }}>
                  CEK SELENGKAPNYA
                  {' '}
                  <FontAwesomeIcon icon={['fad', 'arrow-circle-right'] as IconProp} />
                </a>
              </Link>
            </Col>
          </Row>

          <Row className="justify-content-center">
            <Col md="6" className="text-center">
              <h6 className="text-secondary text-opacity-50 d-block d-sm-none">Mancing Harian</h6>
              <h2 className="d-block d-sm-none mb-3">Mancing Santai Setiap Hari</h2>
              <Image
                src="/img/jadwalharian.png"
                layout="intrinsic"
                width={525}
                height={360}
                className="rounded"
              />
            </Col>
            <Col md="6" className="text-md-start text-center">
              <h6 className="text-secondary text-opacity-50 mt-4 d-none d-md-block">Mancing Harian</h6>
              <h2 className="d-none d-md-block">Mancing Santai Setiap Hari</h2>
              <p className="pe-3">Isi waktu luang anda dengan memancing di Kolam Pemancingan BamboeJaya, mancing harian santai buka setiap hari mulai dari pukul 8 pagi hingga 12 malam.</p>
              <Link href="/jadwal-kegiatan">
                <a className="btn btn-success py-2 mt-md-4" style={{ fontWeight: '600' }}>
                  CEK SELENGKAPNYA
                  {' '}
                  <FontAwesomeIcon icon={['fad', 'arrow-circle-right'] as IconProp} />
                </a>
              </Link>
            </Col>
          </Row>

        </Container>
      </section>

      <Container className="mb-5">
        <Row className="justify-content-center">
          <Col md="8">
            <h1 className="text-center">Testimonial</h1>
            <p className="text-center">Apa kata mereka tentang BamboeJaya</p>
            <Testimonials />
          </Col>
        </Row>
      </Container>

    </MainLayout>
  );
};

export default Home;
